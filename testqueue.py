"""
testqueue.py
"""
import sys
from typing import Dict, List, Union


class QueueFullError(Exception):
    """
    QueueFullError
    """


class QueueEmptyError(Exception):
    """
    QueueEmptyError
    """


class SimplePriorityQueue():
    """
    A simple priority queue.  Assumes an incoming stream of dictionaries
    contains two keys: command to be executed and priority. Priority is an
    integer value from 0 to 10.
    """
    MIN_PRIORITY = 0
    MAX_PRIORITY = 10

    def __init__(self, maxsize: int = 0, items: List[Dict[str, Union[str, int]]] = None) -> None:
        if not isinstance(maxsize, int):
            raise ValueError("maxsize must be an integer")
        if items is not None and not isinstance(items, list):
            raise ValueError("items must be a list or None")
        self.queue = {priority: [] for priority in range(self.MIN_PRIORITY, self.MAX_PRIORITY + 1)}
        self.maxsize = maxsize
        if items:
            for item in items:
                self.put(item)

    def __repr__(self) -> str:
        return "{}({}, {})".format(self.__class__.__name__, self.maxsize, str(self.queue))

    def __str__(self) -> str:
        text = ""
        for priority in range(self.MIN_PRIORITY, self.MAX_PRIORITY + 1):
            for cmd in self.queue[priority]:
                text += f"priority {priority}: {cmd}\n"
        return text.rstrip()

    @staticmethod
    def _is_command_valid(cmd: str) -> None:
        """
        Check if command is valid: string and not empty or None.

        >>> SimplePriorityQueue._is_command_valid("ls")
        >>> SimplePriorityQueue._is_command_valid("")
        Traceback (most recent call last):
        ...
        ValueError: Command cannot be empty
        >>> SimplePriorityQueue._is_command_valid(1)
        Traceback (most recent call last):
        ...
        ValueError: Command must be a string: '1'

        :param str cmd:
        :raise ValueError:
        """
        if not isinstance(cmd, str):
            raise ValueError(f"Command must be a string: '{cmd}'")
        if not cmd:
            raise ValueError("Command cannot be empty")

    @staticmethod
    def _is_priority_valid(priority: int) -> None:
        """
        Check if priority is valid: integer and between 0 and 10.

        >>> SimplePriorityQueue._is_priority_valid(0)
        >>> SimplePriorityQueue._is_priority_valid(10)
        >>> SimplePriorityQueue._is_priority_valid(-1)
        Traceback (most recent call last):
        ...
        ValueError: Priority must be between 0 and 10: -1
        >>> SimplePriorityQueue._is_priority_valid(11)
        Traceback (most recent call last):
        ...
        ValueError: Priority must be between 0 and 10: 11

        :param int priority:
        :raises ValueError:
        """
        if not isinstance(priority, int):
            raise ValueError(f"Priority must be an integer: '{priority}'")
        if not (SimplePriorityQueue.MIN_PRIORITY <= priority <= SimplePriorityQueue.MAX_PRIORITY):
            raise ValueError(f"Priority must be between {SimplePriorityQueue.MIN_PRIORITY} and {SimplePriorityQueue.MAX_PRIORITY}: {priority}")

    def size(self) -> int:
        """
        Returns the total number of items in the queue.

        :rtype: int
        """
        count = 0
        for priority in range(self.MIN_PRIORITY, self.MAX_PRIORITY + 1):
            count += len(self.queue[priority])
        return count

    def put(self, item: Dict[str, Union[str, int]]) -> None:
        """
        Adds one item to the queue.  Items are append to the end of the list
        for each priority level.

        >>> q = SimplePriorityQueue(10);\
            q.put({"command": "ls", "priority": 0});\
            q
        SimplePriorityQueue(10, {0: ['ls'], 1: [], 2: [], 3: [], 4: [], 5: [], 6: [], 7: [], 8: [], 9: [], 10: []})
        >>> q = SimplePriorityQueue(10);\
            q.put({"command": "a", "priority": 1});\
            q.put({"command": "b", "priority": 0});\
            q.put({"command": "c", "priority": 0});\
            print(q)
        priority 0: b
        priority 0: c
        priority 1: a
        >>> SimplePriorityQueue().put({"command": "ls", "priority": 0})
        Traceback (most recent call last):
        ...
        QueueFullError: Queue full

        :param dict(str, int) item:
        :raises QueueFullError:
        :raises TypeError:
        :raises KeyError:
        :raises ValueError:
        """
        if self.full():
            raise QueueFullError("Queue full")
        if not isinstance(item, dict):
            raise TypeError(f"Item must be a dict: {item}")
        if "command" not in item:
            raise KeyError("command")
        if "priority" not in item:
            raise KeyError("priority")
        cmd = item["command"]
        SimplePriorityQueue._is_command_valid(cmd)
        priority = item["priority"]
        SimplePriorityQueue._is_priority_valid(priority)
        self.queue[priority] += [cmd]

    def get(self) -> Union[str, None]:
        """
        Returns and removes one item from the top of the queue.

        >>> SimplePriorityQueue().get()
        Traceback (most recent call last):
        ...
        QueueEmptyError: Queue empty

        :rtype: str or None
        :raises QueueEmptyError:
        """
        if self.empty():
            raise QueueEmptyError("Queue empty")
        for priority in range(self.MIN_PRIORITY, self.MAX_PRIORITY + 1):
            if self.queue[priority]:
                return self.queue[priority].pop(0)
        return None

    def clear(self) -> None:
        """
        Removes all items in the queue.
        """
        for priority in range(self.MIN_PRIORITY, self.MAX_PRIORITY + 1):
            self.queue[priority] = []

    def empty(self) -> bool:
        """
        Returns True if the queue is empty.

        :rtype: bool
        """
        return self.size() == 0

    def full(self) -> bool:
        """
        Returns True if the queue is full.

        :rtype: bool
        """
        return self.size() == self.maxsize


if __name__ == "__main__":
    if "testmod" in sys.argv:
        import doctest
        doctest.testmod(verbose=True if "verbose" in sys.argv else False)
        sys.exit()